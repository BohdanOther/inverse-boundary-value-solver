﻿using IntegralEquations.Math;

namespace IntegralEquations.Solver.KleinGordon.Core
{
    public class ContiniousKernel
    {
        private readonly KleinGordonProblem problem;

        public ContiniousKernel(KleinGordonProblem problem)
        {
            this.problem = problem;
        }

        public double Q1(double[] x, double tj)
        {
            var k = problem.Kappa;
            var y = problem.OuterBound.F.Val(tj.ToVectorArg());
            var r = FunctionHelpers.EuclidianDistance(x, y);

            var kernel = SpecialFunctions.BesselK0(k * r);

            return kernel;
        }

        public double Q2(double[] x, double tj)
        {
            var k = problem.Kappa;
            var y = problem.InnerBound.F.Val(tj.ToVectorArg());
            var r = FunctionHelpers.EuclidianDistance(x, y);
            var xMinusY = FunctionHelpers.VectorDifference(x, y);
            var normal = problem.InnerBound.Normal(tj);

            var kernel = k * SpecialFunctions.BesselK1(k * r) * FunctionHelpers.ScalarProduct(xMinusY, normal) / r;

            return kernel;
        }
    }
}
