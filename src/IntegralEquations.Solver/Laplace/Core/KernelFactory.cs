﻿using IntegralEquations.Math;

namespace IntegralEquations.Solver.Laplace.Core
{
    public class KernelFactory
    {
        private static LaplaceProblem _problem;
        private static int M;

        public static ContiniousKernel BuildContiniousKernel(LaplaceProblem problem, double[] mesh)
        {
            return new ContiniousKernel(_problem);
        }

        public static DiscreteKernel BuildDiskreteKernel(LaplaceProblem problem, double[] mesh)
        {
            _problem = problem;
            M = mesh.Length;
            var kernel = new double[2 * M, 2 * M];

            // calculate discrete kernel values
            for (var i = 0; i < M; i++)
            {
                for (var j = 0; j < M; j++)
                {
                    kernel[i, j] = H11(mesh[i], mesh[j]);
                    kernel[i, j + M] = H12(mesh[i], mesh[j]);
                    kernel[i + M, j] = H21(mesh[i], mesh[j]);
                    kernel[i + M, j + M] = H22(mesh[i], mesh[j]);
                }
            }

            return new DiscreteKernel(kernel);
        }

        private static double H11(double t, double tau)
        {
            var x = _problem.OuterBound.F.Val(t.ToVectorArg());
            var y = _problem.OuterBound.F.Val(tau.ToVectorArg());
            var r = FunctionHelpers.EuclidianDistance(x, y);

            if (!FunctionHelpers.NearlyEquals(t, tau))
                return -System.Math.Log(r);

            // else we have singularity
            var normOfDx = FunctionHelpers.EuclideanNorm(_problem.OuterBound.Derivative.Val(t.ToVectorArg()));

            var singularity = -0.5d * System.Math.Log(normOfDx * normOfDx);
            var giperSingularity = -0.5d * SpecialFunctions.R(t, tau, M);

            return singularity + giperSingularity;

        }
        private static double H12(double t, double tau)
        {
            var x = _problem.OuterBound.F.Val(t.ToVectorArg());
            var y = _problem.InnerBound.F.Val(tau.ToVectorArg());
            var r = FunctionHelpers.EuclidianDistance(x, y);

            return -System.Math.Log(r);
        }
        private static double H21(double t, double tau)
        {
            var x = _problem.InnerBound.F.Val(t.ToVectorArg());
            var y = _problem.OuterBound.F.Val(tau.ToVectorArg());
            var r = FunctionHelpers.EuclidianDistance(x, y);

            return -System.Math.Log(r);
        }
        private static double H22(double t, double tau)
        {
            var x = _problem.InnerBound.F.Val(t.ToVectorArg());
            var y = _problem.InnerBound.F.Val(tau.ToVectorArg());
            var r = FunctionHelpers.EuclidianDistance(x, y);

            if (!FunctionHelpers.NearlyEquals(t, tau))
                return -System.Math.Log(r);

            // else we have singularity
            var normOfDx = FunctionHelpers.EuclideanNorm(_problem.InnerBound.Derivative.Val(t.ToVectorArg()));

            var singularity = -0.5d * System.Math.Log(normOfDx * normOfDx);
            var giperSingularity = -0.5d * SpecialFunctions.R(t, tau, M);

            return singularity + giperSingularity;
        }
    }
}
