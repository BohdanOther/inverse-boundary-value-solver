﻿using IntegralEquations.Math;

namespace IntegralEquations.Solver.Laplace.Core
{
    public class ContiniousKernel
    {
        public LaplaceProblem Problem { get; }

        public ContiniousKernel(LaplaceProblem problem)
        {
            Problem = problem;
        }

        public double Q1(double[] x, double tj)
        {
            var y = Problem.OuterBound.F.Val(tj.ToVectorArg());
            return SpecialFunctions.LaplaceFundamentalSolution(x, y) * Constants.Pi2;
        }

        public double Q2(double[] x, double tj)
        {
            var y = Problem.InnerBound.F.Val(tj.ToVectorArg());
            return SpecialFunctions.LaplaceFundamentalSolution(x, y) * Constants.Pi2;
        }

        public double DerivativeQ11(double t, double tau)
        {
            if (FunctionHelpers.NearlyEquals(t, tau))
            {
                var dx = Problem.OuterBound.Derivative;
                var ddx = Problem.OuterBound.SecondDerivative;
                var dxNorm = FunctionHelpers.EuclideanNorm(dx.Val(tau.ToVectorArg()));

                return FunctionHelpers.ScalarProduct(
                    ddx.Val(t.ToVectorArg()),
                    Problem.OuterBound.Normal(t)
                ) / (2.0 * dxNorm * dxNorm);
            }
            else
            {
                var x = Problem.OuterBound.F.Val(t.ToVectorArg());
                var y = Problem.OuterBound.F.Val(tau.ToVectorArg());
                var xMinusY = FunctionHelpers.VectorDifference(x, y);
                var r = FunctionHelpers.EuclideanNorm(xMinusY);
                var normal = Problem.OuterBound.Normal(t);

                return -FunctionHelpers.ScalarProduct(xMinusY, normal) / (r * r);
            }
        }

        public double DerivativeQ12(double t, double tau)
        {
            var x = Problem.OuterBound.F.Val(t.ToVectorArg());
            var y = Problem.InnerBound.F.Val(tau.ToVectorArg());
            var xMinusY = FunctionHelpers.VectorDifference(x, y);
            var r = FunctionHelpers.EuclideanNorm(xMinusY);
            var normal = Problem.OuterBound.Normal(t);

            return -FunctionHelpers.ScalarProduct(xMinusY, normal) / (r * r);
        }

        public double DerivativeQ21(double t, double tau)
        {
            var x = Problem.InnerBound.F.Val(t.ToVectorArg());
            var y = Problem.OuterBound.F.Val(tau.ToVectorArg());
            var xMinusY = FunctionHelpers.VectorDifference(x, y);
            var r = FunctionHelpers.EuclideanNorm(xMinusY);
            var normal = Problem.InnerBound.Normal(t);

            return -FunctionHelpers.ScalarProduct(xMinusY, normal) / (r * r);
        }

        public double DerivativeQ22(double t, double tau)
        {
            if (FunctionHelpers.NearlyEquals(t, tau))
            {
                var dx = Problem.InnerBound.Derivative;
                var ddx = Problem.InnerBound.SecondDerivative;

                var dxNorm = FunctionHelpers.EuclideanNorm(dx.Val(tau.ToVectorArg()));

                return FunctionHelpers.ScalarProduct(
                    ddx.Val(t.ToVectorArg()),
                    Problem.InnerBound.Normal(t)
                ) / (2.0 * dxNorm * dxNorm);
            }
            else
            {
                var x = Problem.InnerBound.F.Val(t.ToVectorArg());
                var y = Problem.InnerBound.F.Val(tau.ToVectorArg());
                var xMinusY = FunctionHelpers.VectorDifference(x, y);
                var r = FunctionHelpers.EuclideanNorm(xMinusY);
                var normal = Problem.InnerBound.Normal(t);

                return -FunctionHelpers.ScalarProduct(xMinusY, normal) / (r * r);
            }
        }

    }
}
