﻿using IntegralEquations.Math;
using IntegralEquations.Math.Functions;

namespace IntegralEquations.Solver.Laplace
{
    public class LaplaceProblem
    {
        public IScalarFunction OuterFunction { get; set; }
        public IScalarFunction InnerFunction { get; set; }

        public ParametricBoundary OuterBound { get; set; }
        public ParametricBoundary InnerBound { get; set; }


        /// <summary>
        /// Calculates F1 = F1(x11(t), x12(t))
        /// where x1 is outer bound
        /// </summary>
        /// <param name="t">Parameter on outer bount</param>
        /// <returns></returns>
        public virtual double GetOuterFunction(double t)
        {
            var arg = OuterFunction.Dimention == 1
                ? t.ToVectorArg()
                : OuterBound.F.Val(t.ToVectorArg());

            return OuterFunction.Val(arg);
        }

        /// <summary>
        /// Calculates F2 = F2(x21(t), x22(t))
        /// where x2 is inner bound
        /// </summary>
        /// <param name="t">Parameter on inner bount</param>
        /// <returns></returns>
        public virtual double GetInnerFunction(double t)
        {
            var arg = InnerFunction.Dimention == 1
              ? t.ToVectorArg()
              : InnerBound.F.Val(t.ToVectorArg());

            return InnerFunction.Val(arg);
        }
    }
}