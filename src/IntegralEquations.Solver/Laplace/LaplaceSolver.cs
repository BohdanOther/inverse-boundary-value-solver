﻿using IntegralEquations.Solver.Laplace.Core;
using MathNet.Numerics.LinearAlgebra;

namespace IntegralEquations.Solver.Laplace
{
    public class LaplaceSolver
    {
        int M;
        LaplaceProblem problem;

        public LaplaceSolution Solve(LaplaceProblem problem, int m)
        {
            M = m;
            this.problem = problem;

            // equidistant mesh
            var mesh = Vector<double>.Build.Dense(2 * M, j => System.Math.PI * j / M).ToArray();

            var psi = SolvePsi(mesh);

            var kernel = KernelFactory.BuildContiniousKernel(this.problem, mesh);

            return new LaplaceSolution(kernel, psi[0], psi[1], mesh);
        }

        private double[][] SolvePsi(double[] mesh)
        {
            // precalculate H-kernels
            var kernel = KernelFactory.BuildDiskreteKernel(problem, mesh);

            // fill matrixes
            var b = Vector<double>.Build.Dense(4 * M);
            var A = Matrix<double>.Build.Dense(4 * M, 4 * M);

            for (var i = 0; i < 2 * M; i++)
            {
                for (var j = 0; j < 2 * M; j++)
                {
                    A[i, j] = kernel.H11(i, j) / (2.0d * M); // with R
                    A[i, j + 2 * M] = kernel.H12(i, j) / (2.0d * M);
                    A[i + 2 * M, j] = kernel.H21(i, j) / (2.0d * M);
                    A[i + 2 * M, j + 2 * M] = kernel.H22(i, j) / (2.0d * M); // with R
                }

                b[i] = problem.GetOuterFunction(mesh[i]);
                b[i + 2 * M] = problem.GetInnerFunction(mesh[i]);
            }

            // solve system
            var psi = A.Solve(b);
            var psi1 = psi.SubVector(0, 2 * M);
            var psi2 = psi.SubVector(2 * M, 2 * M);

            return new[] { psi1.ToArray(), psi2.ToArray() };
        }
    }
}
