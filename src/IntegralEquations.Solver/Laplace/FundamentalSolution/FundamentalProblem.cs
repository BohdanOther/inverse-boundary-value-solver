﻿using IntegralEquations.Math;

namespace IntegralEquations.Solver.Laplace.FundamentalSolution
{
    public class FundamentalProblem : LaplaceProblem
    {
        private readonly double[] y;

        /// <summary>
        /// Point outside region D
        /// </summary>
        /// <param name="y"></param>
        public FundamentalProblem(double[] y)
        {
            this.y = y;
        }

        public override double GetOuterFunction(double t)
        {
            var x = OuterBound.F.Val(t.ToVectorArg());
            return SpecialFunctions.LaplaceFundamentalSolution(x, y);
        }

        public override double GetInnerFunction(double t)
        {
            var x = InnerBound.F.Val(t.ToVectorArg());
            return SpecialFunctions.LaplaceFundamentalSolution(x, y);
        }
    }
}
