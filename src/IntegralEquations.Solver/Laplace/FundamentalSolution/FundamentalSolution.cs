﻿
using IntegralEquations.Math;
using IntegralEquations.Math.Functions;

namespace IntegralEquations.Solver.Laplace.FundamentalSolution
{
    /// <summary>
    /// Exact solution for special case
    /// when u(x) = Φ(x, y*),
    /// where y* is fixed point outside region D
    /// </summary>
    public class FundamentalSolution : IScalarFunction
    {
        public int Dimention => 2;
        public LaplaceProblem Problem { get; }


        private readonly double[] y;


        public FundamentalSolution(ParametricBoundary outerBound, ParametricBoundary innerBound, double[] y)
        {
            this.y = y;

            Problem = new FundamentalProblem(y)
            {
                OuterBound = outerBound,
                InnerBound = innerBound
            };
        }

        public double Val(double[] args)
        {
            return SpecialFunctions.LaplaceFundamentalSolution(args, y);
        }

        public double Val(double x)
        {
            return Val(new[] { x, 0 });
        }
    }
}
