﻿using IntegralEquations.Math;
using IntegralEquations.Math.Functions;
using IntegralEquations.Solver.Laplace.Core;
using System;

namespace IntegralEquations.Solver.Laplace
{
    public class LaplaceSolution : IScalarFunction
    {
        public int Dimention => 2;
        public double[] Mesh { get; }

        private readonly int M;
        /// <summary>
        /// Density on OuterBoundary
        /// </summary>
        private readonly double[] psi1;
        /// <summary>
        /// Density on InnerBoundary 
        /// </summary>
        private readonly double[] psi2;
        private readonly ContiniousKernel kernel;

        public LaplaceSolution(ContiniousKernel kernel, double[] psi1, double[] psi2, double[] mesh)
        {
            if (psi1.Length != psi2.Length)
                throw new Exception("Sizes of psi vecors are different");
            if (psi1.Length != mesh.Length)
                throw new Exception("Wrong mesh size");

            M = mesh.Length / 2;

            this.kernel = kernel;
            this.psi1 = psi1;
            this.psi2 = psi2;
            this.Mesh = mesh;
        }


        private double GetVal(double[] x)
        {
            var val = 0.0;
            for (var i = 0; i < 2 * M; i++)
            {
                val += psi1[i] * kernel.Q1(x, Mesh[i]) +
                       psi2[i] * kernel.Q2(x, Mesh[i]);
            }

            return val / (2 * M);
        }

        public double Val(double[] args)
        {
            return GetVal(args);
        }

        public double Val(double x)
        {
            return Val(new[] { x, x });
        }

        public double[] NormalDerivativeOuterBound()
        {
            //Contains NormalDerivative values at mesh points
            var dtVector = new double[2 * M];
            for (var i = 0; i < 2 * M; i++)
            {
                var t = Mesh[i];

                var val = 0.0;
                for (var j = 0; j < 2 * M; j++)
                {
                    var tau = Mesh[j];
                    val += psi1[j] * kernel.DerivativeQ11(t, tau) / (2 * M) +
                           psi2[j] * kernel.DerivativeQ12(t, tau) / (2 * M);
                }

                var boundDerivative = kernel.Problem.OuterBound.Derivative.Val(t.ToVectorArg());
                val += psi1[i] / (-2.0 * FunctionHelpers.EuclideanNorm(boundDerivative));

                dtVector[i] = val;
            }

            return dtVector;
        }

        public double[] NormalDerivativeInnerBound()
        {
            var dt = new double[2 * M];
            for (var i = 0; i < 2 * M; i++)
            {
                var t = Mesh[i];

                var val = 0.0;
                for (var j = 0; j < 2 * M; j++)
                {
                    var tau = Mesh[j];
                    val += psi1[j] * kernel.DerivativeQ21(t, tau) / (2 * M) +
                           psi2[j] * kernel.DerivativeQ22(t, tau) / (2 * M);
                }

                var boundDerivative = kernel.Problem.InnerBound.Derivative.Val(t.ToVectorArg());
                val += psi2[i] / (-2.0 * FunctionHelpers.EuclideanNorm(boundDerivative));

                dt[i] = val;
            }

            return dt;
        }
    }
}
