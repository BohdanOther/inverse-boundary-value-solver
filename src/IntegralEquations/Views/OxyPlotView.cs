﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.WindowsForms;
using System;
using System.Windows.Forms;

namespace cbspline
{
    public partial class OxyPlotView : UserControl
    {
        public event EventHandler RefreshPlot;

        public PlotView PlotView { get; private set; }
        public PlotModel Model => PlotView.Model;

        public void SetModel(PlotModel model)
        {
            PlotView.Model = model;
        }

        public PlotModel AxesPlotModel
        {
            get
            {
                var axesPlotModel = new PlotModel();

                var linearAxis1 = new LinearAxis
                {
                    MajorGridlineStyle = LineStyle.Solid,
                    MinorGridlineStyle = LineStyle.Dot
                };
                axesPlotModel.Axes.Add(linearAxis1);

                var linearAxis2 = new LinearAxis
                {
                    MajorGridlineStyle = LineStyle.Solid,
                    MinorGridlineStyle = LineStyle.Dot,
                    Position = AxisPosition.Bottom
                };

                axesPlotModel.Axes.Add(linearAxis2);

                return axesPlotModel;
            }
        }

        public OxyPlotView()
        {
            InitializeComponent();
            PlotView = new PlotView { Dock = DockStyle.Fill, Model = AxesPlotModel };
        }

        public void Redraw()
        {
            PlotView.Model.InvalidatePlot(false);
        }

        private void OxyPlotView_Load(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                PlotView = new PlotView { Dock = DockStyle.Fill, Model = AxesPlotModel };
            }
            this.Controls.Add(PlotView);
        }

        public void CreateMovablePlotModel(LineSeries movableSeries, PlotModel baseModel = null)
        {
            var model = baseModel ?? new PlotModel();
            model.Series.Add(movableSeries);

            var indexOfPointToMove = -1;

            // Subscribe to the mouse down event on the line series
            movableSeries.MouseDown += (s, e) =>
            {
                var indexOfNearestPoint = (int)System.Math.Round(e.HitTestResult.Index);
                var nearestPoint = movableSeries.Transform(movableSeries.Points[indexOfNearestPoint]);

                switch (e.ChangedButton)
                {
                    case OxyMouseButton.Right:
                        // Check if we are near a point
                        if ((nearestPoint - e.Position).Length < 10)
                        {
                            movableSeries.Points.RemoveAt(indexOfNearestPoint);
                        }
                        break;

                    case OxyMouseButton.Left:
                        // Check if we are near a point
                        if ((nearestPoint - e.Position).Length < 10)
                        {
                            // Start editing this point
                            indexOfPointToMove = indexOfNearestPoint;
                        }
                        else
                        {
                            // otherwise create a point on the current line segment
                            var i = (int)e.HitTestResult.Index + 1;
                            movableSeries.Points.Insert(i, movableSeries.InverseTransform(e.Position));
                            indexOfPointToMove = i;
                        }
                        break;
                }

                OnRefreshPlot();

                model.InvalidatePlot(false);

                e.Handled = true;
            };

            movableSeries.MouseMove += (s, e) =>
            {
                if (indexOfPointToMove >= 0)
                {
                    // Move the point being edited.
                    movableSeries.Points[indexOfPointToMove] = movableSeries.InverseTransform(e.Position);
                    movableSeries.MarkerType = MarkerType.Square;

                    // continious update
                    OnRefreshPlot();

                    model.InvalidatePlot(false);
                    e.Handled = true;
                }
            };

            movableSeries.MouseUp += (s, e) =>
            {
                // Stop editing
                indexOfPointToMove = -1;
                movableSeries.MarkerType = MarkerType.Circle;
                OnRefreshPlot();
                model.InvalidatePlot(false);
                e.Handled = true;
            };

            model.MouseDown += (s, e) =>
            {
                if (e.ChangedButton == OxyMouseButton.Left)
                {
                    // Add a point to the line series.
                    var p = movableSeries.InverseTransform(e.Position);
                    movableSeries.Points.Add(p);
                    indexOfPointToMove = movableSeries.Points.Count - 1;
                    OnRefreshPlot();
                    model.InvalidatePlot(false);
                    e.Handled = true;
                }
            };

            PlotView.Model = model;
        }

        private void OnRefreshPlot()
        {
            RefreshPlot?.Invoke(PlotView, new EventArgs());
        }
    }
}
