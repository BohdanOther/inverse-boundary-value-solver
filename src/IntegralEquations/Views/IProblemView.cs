﻿using IntegralEquations.Solver;
using IntegralEquations.Solver.KleinGordon;
using IntegralEquations.Solver.Laplace;

namespace IntegralEquations.Views
{
    public interface IProblemView
    {
        LaplaceProblem Problem { get; }
    }
}
