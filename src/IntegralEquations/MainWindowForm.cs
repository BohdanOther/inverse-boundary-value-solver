﻿using BoundaryReconstruction;
using cbspline;
using IntegralEquations.Math;
using IntegralEquations.Math.Functions;
using IntegralEquations.Plot3D;
using IntegralEquations.Solver.Laplace;
using IntegralEquations.Solver.Laplace.FundamentalSolution;
using IntegralEquations.UI.Controls;
using IntegralEquations.Views;
using MathNet.Numerics;
using nzy3D.Chart;
using nzy3D.Chart.Controllers.Thread.Camera;
using nzy3D.Colors;
using nzy3D.Colors.ColorMaps;
using nzy3D.Plot3D.Builder;
using nzy3D.Plot3D.Builder.Concrete;
using nzy3D.Plot3D.Primitives;
using nzy3D.Plot3D.Primitives.Axes.Layout;
using nzy3D.Plot3D.Rendering.Canvas;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Linq;
using System.Windows.Forms;
using static System.Math;
using Constants = IntegralEquations.Math.Constants;
using SpecialFunctions = IntegralEquations.Math.SpecialFunctions;

namespace IntegralEquations
{
    public partial class MainWindowForm : Form
    {
        private CameraThreadController controller;
        private IAxeLayout axeLayout;

        Chart chart;

        private IProblemView problemView;

        FundamentalSolution exact;

        private Shape surface;
        private OxyPlotView plotView;

        public MainWindowForm()
        {
            Math.UseExamples.Test();
            InitializeComponent();

            ToolStripManager.Renderer = new DarkToolStripRenderer();

            plotView = this.oxyPlot;
            //var spline = new LineSeries
            //{
            //    Color = OxyColor.FromRgb(211, 47, 47),
            //    StrokeThickness = 4
            //};


        }

        private void InitRenderer(IScalarFunction f)
        {
            // Create a range for the graph generation
            var range = new nzy3D.Maths.Range(-1, 1);
            var steps = 50;

            // Build a nice surface to display with cool alpha colors 
            // (alpha 0.8 for surface color and 0.5 for wireframe)
            // solution here
            surface = Builder.buildRing(new OrthonormalGrid(range, steps, range, steps), new FunctionMapper(f), 0.5f, 1);

            surface.ColorMapper = new ColorMapper(new ColorMapRainbow(), surface.Bounds.zmin, surface.Bounds.zmax, new Color(1, 1, 1, 0.8));
            surface.FaceDisplayed = true;
            surface.WireframeDisplayed = true;
            surface.WireframeColor = Color.CYAN;
            surface.WireframeColor.mul(new Color(1, 1, 1, 0.5));
            // Create the chart and embed the surface within

            chart = new Chart(renderer, Quality.Nicest);
            chart.Scene.Graph.Add(surface);
            axeLayout = chart.AxeLayout;
            axeLayout.MainColor = Color.WHITE;
            chart.View.BackgroundColor = Color.GRAY;

            // Create a mouse control
            var mouse = new nzy3D.Chart.Controllers.Mouse.Camera.CameraMouseController();
            mouse.addControllerEventListener(renderer);
            chart.addController(mouse);

            // This is just to ensure code is reentrant (used when code is not called in Form_Load but another reentrant event)
            DisposeBackgroundThread();

            // Create a thread to control the camera based on mouse movements
            controller = new CameraThreadController();
            controller.addControllerEventListener(renderer);
            mouse.addSlaveThreadController(controller);
            chart.addController(controller);
            controller.Start();
            // Associate the chart with current control
            renderer.setView(chart.View);

            this.Refresh();
        }

        private void DisposeBackgroundThread()
        {
            controller?.Dispose();
        }

        private void MainWindowForm_Load(object sender, EventArgs e)
        {
            if (chbUseFundamentalProblem.Checked)
                problemView = new FundamentalProblemView();
            else
                problemView = new ProblemView();
        }

        private void MainWindowForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DisposeBackgroundThread();
        }

        private void SwitchProblemPanels()
        {
            panelProblemView.SuspendLayout();

            panelProblemView.Controls.Clear();
            if (chbUseFundamentalProblem.Checked)
            {
                var c = new FundamentalProblemView();
                problemView = c;
                panelProblemView.Controls.Add(c);
            }
            else
            {
                var c = new ProblemView();
                problemView = c;
                panelProblemView.Controls.Add(c);
            }

            panelProblemView.ResumeLayout(true);

        }

        private void chbUseFundamentalProblem_Click(object sender, EventArgs e)
        {
            SwitchProblemPanels();
        }

        private void btnSolve_Click(object sender, EventArgs e)
        {
            // super-ellipse , squircle
            //var gamma2 = new ParametricFunction(
            // new ScalarFunctionHandle(t => 0.5 * Sqrt(Abs(Cos(t[0]))) * Sign(Cos(t[0])), 1),
            //     new ScalarFunctionHandle(t => 1 * Sqrt(Abs(Sin(t[0]))) * Sign(Sin(t[0])), 1)
            // );

            // Epitrochoid
            //var gamma1 = new ParametricFunction( // super-ellipse , squircle
            //    new ScalarFunctionHandle(t => 6 * Cos(t[0]) - 0.5 * Cos(5 * t[0]), 1),
            //        new ScalarFunctionHandle(t => 6 * Sin(t[0]) - 0.5 * Sin(4 * t[0]), 1)
            //    );

            // var gamma1 = new ParametricFunction(
            //new ScalarFunctionHandle(t => 8 * Sqrt(Abs(Cos(t[0]))) * Sign(Cos(t[0])), 1),
            //    new ScalarFunctionHandle(t => 4 * Sqrt(Abs(Sin(t[0]))) * Sign(Sin(t[0])), 1)
            //);

            var gamma1 = ParametricBoundary.BuildCircle(1);
            var gamma2 = ParametricBoundary.BuildCircle(0.5);

            var y = new[] { 0.0, 0.0 };
            exact = new FundamentalSolution(gamma1, gamma2, y);

            var problem = new LaplaceProblem
            {
                OuterBound = gamma1,
                InnerBound = gamma2,
                // F1 = new ScalarFunctionHandle(x => x[0] * x[0] - x[1] * x[1], 2),
                // F2 = new ScalarFunctionHandle(x => x[0] + x[1], 2),
                OuterFunction = new ScalarFunctionHandle(x => x[0] * x[0], 2),
                InnerFunction = new ScalarFunctionHandle(x => 3, 2),
            };


            var solution = new LaplaceSolver().Solve(exact.Problem, 128);
            //solution = solver.Solve(problem, 2);
            var mesh = solution.Mesh;

            // Computed outer normal derivative
            var dt = solution.NormalDerivativeOuterBound();

            // Analytic outer normal derivative
            var exactDt = new double[mesh.Length];

            for (int i = 0; i < mesh.Length; i++)
            {
                var t = mesh[i];
                exactDt[i] = SpecialFunctions.LaplaceFundamentalSolutionNormalDerivative(
                    gamma1.F.Val(t.ToVectorArg()),
                    y,
                    gamma1.Normal(t)
                );
            }

            InitRenderer(solution);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            var arr = textBox1.Text.Split(',').Select(double.Parse).ToArray();

            //var exactf = solver.Solve(problem, 128).Val(arr);
            var exactf = exact.Val(arr);

            var n = new[] { 2, 4, 8, 16, 32, 64, 128, 256 };

            var solver = new LaplaceSolver();
            foreach (var v in n)
            {
                //var p = solver.Solve(problem, v);
                var p = solver.Solve(exact.Problem, v);
                var err = Abs(p.Val(arr) - exactf);
                dataGridView1.Rows.Add(v, err.ToString("N7"));
            }
        }

        private void darkButton1_Click(object sender, EventArgs e)
        {
            // solve direct problem to form inverse problem
            // with known heat flux
            var laplaceSolver = new LaplaceSolver();
            var laplaceProblem = new LaplaceProblem
            {
                OuterBound = ParametricBoundary.BuildCircle(1),
                OuterFunction = new ScalarFunctionHandle(x => x[0] * x[0], 2),

                InnerFunction = new ScalarFunctionHandle(x => 0, 2),
                InnerBound = new ParametricBoundary(
                    new ParametricFunction(
                        new ScalarFunctionHandle(t => (0.4 * Cos(t[0]) * Cos(t[0]) + 0.16 * Sin(t[0] + 0.5) * Sin(t[0] + 0.5)) * Cos(t[0]), 1),
                        new ScalarFunctionHandle(t => (0.4 * Cos(t[0]) * Cos(t[0]) + 0.16 * Sin(t[0] + 0.5) * Sin(t[0] + 0.5)) * Sin(t[0]), 1)
                    )
                )
            };

            var laplace = laplaceSolver.Solve(laplaceProblem, 32);
            InitRenderer(laplace);
            var g = Interpolate.PolynomialEquidistant(
                laplace.Mesh,
                laplace.NormalDerivativeOuterBound()
            );

            // set up inverse problem
            var problem = new BoundaryReconstructionProblem
            {
                RegularizationParameter = 0.0001,
                NumberOfValuePoints = 32,
                LaplaceCollocationPoints = 32,
                NumberOfCollocationPoints = 4,

                BasisGenerator = i => new ScalarFunctionHandle(x => Cos(x[0]*i), 1),

                OuterBoundary = ParametricBoundary.BuildCircle(1),
                DirichletConditionOuter = new ScalarFunctionHandle(x => x[0] * x[0], 2),
                NeumanConditionOuter = new ScalarFunctionHandle(x => g.Interpolate(x[0]), 1),

                InitialGuessInnerRadial = new ScalarFunctionHandle(x =>
                {
                    var t = x[0];
                    return 0.2 * Cos(t) * Cos(t) + 0.08 * Sin(t + 0.5) * Sin(t + 0.5);
                }, 1)
            };

            var solver = new BoundaryReconstructionSolver(problem);
            var solution = solver.Solve();

            PlotBoundary(solution, problem);
        }

        private void PlotBoundary(BoundaryReconstructionSolution solution, BoundaryReconstructionProblem problem)
        {
            // plot results
            var outerBoundary = new FunctionSeries(
                t => problem.OuterBoundary.F.F1.Val(t.ToVectorArg()),
                t => problem.OuterBoundary.F.F2.Val(t.ToVectorArg()),
                0,
                Constants.Pi2,
                100,
                "Boundary");

            // big bob
            var innerBoundary = new FunctionSeries(
                t => (0.2 * Cos(t) * Cos(t) + 0.08 * Sin(t + 0.5) * Sin(t + 0.5)) * Cos(t),
                t => (0.2 * Cos(t) * Cos(t) + 0.08 * Sin(t + 0.5) * Sin(t + 0.5)) * Sin(t),
                0.0,
                Constants.Pi2,
                100,
                "Start"
                )
            {
                LineStyle = LineStyle.Dash
            };

            // big bob
            var innerBoundary2 = new FunctionSeries(
                t => (0.4 * Cos(t) * Cos(t) + 0.16 * Sin(t + 0.5) * Sin(t + 0.5)) * Cos(t),
                t => (0.4 * Cos(t) * Cos(t) + 0.16 * Sin(t + 0.5) * Sin(t + 0.5)) * Sin(t),
                0.0,
                Constants.Pi2,
                100,
                "Origin"
                )
            {
                LineStyle = LineStyle.Dash
            };

            for (int i = 0; i < solution.BoundaryIterations.Count; i++)
            {
                var b = solution.BoundaryIterations[i];

                plotView.Model.Series.Add(new FunctionSeries(
                    t => b.F.F1.Val(t.ToVectorArg()),
                    t => b.F.F2.Val(t.ToVectorArg()),
                    0,
                    Constants.Pi2,
                    100,
                    "Reconstructed [k=" + i + "]")
                );
            }

            plotView.Model.Series.Add(outerBoundary);
            plotView.Model.Series.Add(innerBoundary);
            plotView.Model.Series.Add(innerBoundary2);

            plotView.Redraw();
        }

        private void darkTabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var tab = sender as TabControl;

            if (tab.SelectedIndex == 2)
            {
                renderer.Visible = false;
                oxyPlot.Visible = true;
            }
            else
            {
                renderer.Visible = true;
                oxyPlot.Visible = false;
            }
        }
    }
}
