﻿using IntegralEquations.Math;
using IntegralEquations.Math.Functions;
using System;

namespace BoundaryReconstruction
{
    public class BoundaryReconstructionProblem
    {
        public double RegularizationParameter { get; set; }

        /// <summary>
        /// Represents basis size
        /// </summary>
        public int NumberOfCollocationPoints { get; set; }
        public int NumberOfValuePoints { get; set; }
        public int LaplaceCollocationPoints { get; set; }


        public IScalarFunction DirichletConditionOuter { get; set; }
        public IScalarFunction NeumanConditionOuter { get; set; }
        public ParametricBoundary OuterBoundary { get; set; }


        /// <summary>
        /// Initial guess of unknown radial function 
        /// which defines inner boundary form.
        /// </summary>
        public IScalarFunction InitialGuessInnerRadial { get; set; }
        public Func<int, IScalarFunction> BasisGenerator { get; set; }
    }
}
