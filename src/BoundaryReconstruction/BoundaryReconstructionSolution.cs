﻿using IntegralEquations.Math;
using IntegralEquations.Math.Functions;
using MathNet.Numerics.LinearAlgebra;
using System.Collections.Generic;

namespace BoundaryReconstruction
{
    public class BoundaryReconstructionSolution
    {
        public IScalarFunction RadialFunction { get; set; }
        public ParametricBoundary ReconstructedBoundary { get; set; }

        public List<Vector<double>> RadialCoefficientIterations { get; set; }
        public List<IScalarFunction> RadialIterations { get; set; } 
        public List<ParametricBoundary> BoundaryIterations { get; set; }
    }
}
