﻿using IntegralEquations.Math;
using IntegralEquations.Math.Functions;
using IntegralEquations.Solver.Laplace;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using Constants = IntegralEquations.Math.Constants;

namespace BoundaryReconstruction
{
    public class BoundaryReconstructionSolver
    {
        private static readonly ScalarFunctionHandle Zero = new ScalarFunctionHandle(x => 0, 2);

        private readonly BoundaryReconstructionProblem problem;
        private readonly IScalarFunction[] basis;

        public BoundaryReconstructionSolver(BoundaryReconstructionProblem problem)
        {
            if (problem.NumberOfValuePoints <= problem.NumberOfCollocationPoints)
                throw new ArgumentException("Number of value points must be greater than number of collocation points");

            this.problem = problem;

            // set up test basis
            basis = new IScalarFunction[problem.NumberOfCollocationPoints];
            for (var i = 0; i < problem.NumberOfCollocationPoints; i++)
                basis[i] = problem.BasisGenerator(i);
        }

        public BoundaryReconstructionSolution Solve()
        {
            // TODO: derivatives in normal are computed numericaly here
            var innerBoundary = new ParametricBoundary(
                new ParametricFunction(
                    new ScalarFunctionHandle(x => problem.InitialGuessInnerRadial.Val(x) * Math.Cos(x[0]), 1),
                    new ScalarFunctionHandle(x => problem.InitialGuessInnerRadial.Val(x) * Math.Sin(x[0]), 1)
                    )
                );

            var solution = new BoundaryReconstructionSolution
            {
                BoundaryIterations = new List<ParametricBoundary>
                {
                    innerBoundary
                },
                RadialCoefficientIterations = new List<Vector<double>>
                {
                    Vector<double>.Build.Dense(problem.NumberOfCollocationPoints)
                },
                RadialIterations = new List<IScalarFunction>()
            };


            for (var i = 0; i < 5; i++)
            {
                var q = PerformIterationStep(solution.BoundaryIterations[i]);


                var radialIteration = solution.RadialCoefficientIterations[i].Add(q);
                solution.RadialCoefficientIterations.Add(radialIteration);


                var currentInnerRadial = BuildRadial(radialIteration);
                solution.RadialIterations.Add(currentInnerRadial);


                var boundary = new ParametricBoundary(
                    new ParametricFunction(
                        new ScalarFunctionHandle(
                            x => (problem.InitialGuessInnerRadial.Val(x) + currentInnerRadial.Val(x)) * Math.Cos(x[0]), 1),
                        new ScalarFunctionHandle(
                            x => (problem.InitialGuessInnerRadial.Val(x) + currentInnerRadial.Val(x)) * Math.Sin(x[0]), 1)
                        )
                    );

                solution.BoundaryIterations.Add(boundary);
                solution.ReconstructedBoundary = boundary;
            }

            return solution;
        }

        private IScalarFunction BuildRadial(Vector<double> q)
        {
            return new ScalarFunctionHandle(t =>
            {
                var val = 0.0;
                for (var i = 0; i < basis.Length; i++)
                {
                    val += q[i] * basis[i].Val(t);
                }

                return val;
            }, 1);
        }

        private Vector<double> PerformIterationStep(ParametricBoundary innerBoundary)
        {
            var A = Matrix<double>.Build.Dense(problem.NumberOfValuePoints, problem.NumberOfCollocationPoints);
            var b = Vector<double>.Build.Dense(problem.NumberOfValuePoints);

            // Generate equidistand mesh on [0, 2π].
            // It better to match the mesh in Laplace problems we're going to solve. 
            // See LaplaceCollocationPoints
            var mesh = Vector<double>.Build.Dense(problem.NumberOfValuePoints, j => Constants.Pi2 * j / problem.NumberOfValuePoints).ToArray();

            // Solve first direct problem to get operator F 
            // and normal derivative of F in order to solve the second problem.
            var directProblemSolution = SolveFirstDirectProblem(innerBoundary);

            // We can precomute all F operator results on given mesh.
            var mainOperatorValues = PrecomputeMainOperatorValues(directProblemSolution, mesh);

            // We can precomoute F' operators, because they're the same for all matrices
            // but we need results in different points.
            var frechetDerivativeOperators = PrecomputeFrechetDerivatives(directProblemSolution, innerBoundary);

            // Can precompute G(s[j]) on mesh.
            // var flux = new double[problem.NumberOfValuePoints];

            for (var i = 0; i < problem.NumberOfValuePoints; i++)
            {
                var si = mesh[i].ToVectorArg();

                for (var j = 0; j < problem.NumberOfCollocationPoints; j++)
                {
                    A[i, j] = frechetDerivativeOperators[j].Val(si);
                }

                b[i] = problem.NeumanConditionOuter.Val(si) - mainOperatorValues[i];
            }

            var x = LinearSystemSolver.SolveTikhonovLeastSquares(A, b, problem.RegularizationParameter);

            return x;
        }

        /// <summary>
        /// Operator values are actually laplaceSolution outer boundary normal derivative values
        /// </summary>
        private double[] PrecomputeMainOperatorValues(LaplaceSolution directProblemSolution, double[] mesh)
        {
            var solutionNormalDerivativeOuter = Interpolate.PolynomialEquidistant(
                directProblemSolution.Mesh,
                directProblemSolution.NormalDerivativeOuterBound()
            );

            var res = new double[mesh.Length];
            for (var i = 0; i < mesh.Length; i++)
            {
                res[i] = solutionNormalDerivativeOuter.Interpolate(mesh[i]);
            }

            return res;
        }

        private IScalarFunction[] PrecomputeFrechetDerivatives(LaplaceSolution directProblemSolution, ParametricBoundary innerBoundary)
        {
            var frechetDerivativeOperators = new IScalarFunction[problem.NumberOfCollocationPoints];

            for (var i = 0; i < problem.NumberOfCollocationPoints; i++)
            {
                // Solve second problem using computed F'
                var secondProblemSolution = SolveSecondProblem(directProblemSolution, innerBoundary, basis[i]);

                // interpotate w' normal derivative on outer boundary
                var dw = Interpolate.PolynomialEquidistant(
                    secondProblemSolution.Mesh,
                    secondProblemSolution.NormalDerivativeOuterBound()
                );

                frechetDerivativeOperators[i] = new ScalarFunctionHandle(x => dw.Interpolate(x[0]), 1);
            };

            return frechetDerivativeOperators;
        }

        private LaplaceSolution SolveFirstDirectProblem(ParametricBoundary innerBoundary)
        {
            var laplaceSolver = new LaplaceSolver();
            var laplaceProblem = new LaplaceProblem
            {
                OuterBound = problem.OuterBoundary,
                OuterFunction = problem.DirichletConditionOuter,

                InnerBound = innerBoundary,
                InnerFunction = Zero
            };

            return laplaceSolver.Solve(laplaceProblem, problem.LaplaceCollocationPoints);
        }

        private LaplaceSolution SolveSecondProblem(LaplaceSolution directProblemSolution,
            ParametricBoundary innerBoundary, IScalarFunction basisFunction)
        {
            var directSolutionNormalDerivativeInner = Interpolate.PolynomialEquidistant(
                directProblemSolution.Mesh,
                directProblemSolution.NormalDerivativeInnerBound()
            );

            var laplaceSolver = new LaplaceSolver();
            var laplaceProblem = new LaplaceProblem
            {
                OuterBound = problem.OuterBoundary,
                OuterFunction = Zero,

                InnerBound = innerBoundary,
                InnerFunction = new ScalarFunctionHandle(x =>
                {
                    var t = x[0];
                    var b = basisFunction.Val(t.ToVectorArg());
                    var normal = innerBoundary.Normal(t);

                    // TODO: is this correct ?
                    var a = new double[] { b * Math.Cos(t), b * Math.Sin(t) };

                    // TODO: check the sign here
                    return -FunctionHelpers.ScalarProduct(a, normal) * directSolutionNormalDerivativeInner.Interpolate(t);
                }, 1)
            };

            return laplaceSolver.Solve(laplaceProblem, problem.LaplaceCollocationPoints);
        }
    }
}
