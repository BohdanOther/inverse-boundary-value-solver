﻿namespace IntegralEquations.Math
{
    public static class Constants
    {
        /// <summary>
        ///  Euler–Mascheroni constant.
        ///  Defined as the limiting difference between the harmonic series and the natural logarithm
        /// </summary>
        public const double Gamma = 0.5772156649;

        /// <summary>
        /// Pi*2
        /// </summary>
        public const double Pi2 = System.Math.PI * 2;

        /// <summary>
        /// Tollerance number
        /// </summary>
        public const double Eps = 0.00001;
    }
}
