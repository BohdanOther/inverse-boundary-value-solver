﻿using MathNet.Numerics.LinearAlgebra;

namespace IntegralEquations.Math
{
    public static class LinearSystemSolver
    {
        public static Vector<double> SolveLeastSquares(Matrix<double> a, Vector<double> b)
        {
            return a
                .TransposeThisAndMultiply(a)
                .Inverse()
                .Multiply(a.TransposeThisAndMultiply(b));
        }

        public static Vector<double> SolveTikhonovLeastSquares(Matrix<double> a, Vector<double> b, double regularization, Vector<double> weights = null)
        {
            weights = weights ?? CreateVector.Dense(a.ColumnCount, 1.0);
            var tikhonovMatrix = CreateMatrix.DenseOfDiagonalVector(weights) * regularization;

            // x = [(A*A + T)^-1] x [A*b]
            return a
                .TransposeThisAndMultiply(a)
                .Add(tikhonovMatrix)
                .Inverse()
                .Multiply(a.TransposeThisAndMultiply(b));
        }
    }
}
