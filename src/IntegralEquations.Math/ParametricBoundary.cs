﻿using IntegralEquations.Math.Functions;

namespace IntegralEquations.Math
{
    public class ParametricBoundary
    {
        public static ParametricBoundary BuildCircle(double radius)
        {
            return new ParametricBoundary(
                // function
                new ParametricFunction(
                    new ScalarFunctionHandle(x => radius * System.Math.Cos(x[0]), 1),
                    new ScalarFunctionHandle(x => radius * System.Math.Sin(x[0]), 1)

                ),

                // derivative
                new ParametricFunction(
                    new ScalarFunctionHandle(x => -radius * System.Math.Sin(x[0]), 1),
                    new ScalarFunctionHandle(x => radius * System.Math.Cos(x[0]), 1)
                ),

                // second derivative
                new ParametricFunction(
                    new ScalarFunctionHandle(x => -radius * System.Math.Cos(x[0]), 1),
                    new ScalarFunctionHandle(x => -radius * System.Math.Sin(x[0]), 1)
                )
            );
        }

        public ParametricFunction F { get; }
        public ParametricFunction Derivative { get; }
        public ParametricFunction SecondDerivative { get; }

        public ParametricBoundary(ParametricFunction f, ParametricFunction df = null, ParametricFunction ddf = null)
        {
            F = f;

            Derivative = df ?? new ParametricFunction(
                f.F1.Derivative(),
                f.F2.Derivative()
            );

            SecondDerivative = ddf ?? new ParametricFunction(
                f.F1.Derivative().Derivative(),
                f.F2.Derivative().Derivative()
            );
        }

        /// <summary>
        /// Outer normalized normal
        /// </summary>
        public double[] Normal(double t)
        {
            var arg = t.ToVectorArg();
            var norm = FunctionHelpers.EuclideanNorm(Derivative.Val(arg));

            var res = new[]
            {
                Derivative.F2.Val(arg) / norm,
                -Derivative.F1.Val(arg) / norm
            };

            return res;
        }
    }
}
