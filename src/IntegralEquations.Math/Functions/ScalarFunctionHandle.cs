﻿using System;

namespace IntegralEquations.Math.Functions
{
    public class ScalarFunctionHandle : IScalarFunction
    {
        public int Dimention { get; }

        private readonly Func<double[], double> f;

        public ScalarFunctionHandle(Func<double[], double> f, int dim)
        {
            this.f = f;
            Dimention = dim;
        }

        public double Val(double[] args)
        {
            if (args.Length != Dimention)
                throw new ArgumentException($"Dimension of argument must be {Dimention}");

            return f(args);
        }
    }
}
