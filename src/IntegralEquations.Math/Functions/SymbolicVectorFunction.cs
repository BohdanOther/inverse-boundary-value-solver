﻿using System;
using NCalc;

namespace IntegralEquations.Math.Functions
{
    public class SymbolicVectorFunction : IVectorFunction
    {
        public int Dimention { get; }

        private readonly Expression[] expr;
        private readonly string[] args;

        /// <summary>
        /// Multivariable multidimentional symbolic function evaluator
        /// For dim = n will be x1, x2, ..., xn arguments 
        /// </summary>
        /// <param name="expr">Symbolic expressions to evaluate</param>
        /// <param name="dim">Number of variables</param>
        /// <param name="arg">Name of variable. By default 'x'</param>
        public SymbolicVectorFunction(string[] expr, int dim, string arg = "x")
        {
            if (dim < 1)
                throw new ArgumentException("Dimention must be grater 0");

            Dimention = dim;

            this.expr = new Expression[expr.Length];
            for (var i = 0; i < expr.Length; i++)
            {
                this.expr[i] = new Expression(expr[i].Replace(',', '.'));
            }

            args = new string[dim];
            if (dim == 1) args[0] = arg;
            else
            {
                for (var i = 1; i <= dim; i++)
                {
                    args[i - 1] = $"{arg}{i}";
                }
            }
        }

        public double[] Val(double[] args)
        {
            // guard
            if (args.Length != Dimention)
                throw new ArgumentException($"Dimension of argument must be {Dimention}");

            // fill arguments with values
            for (var i = 0; i < expr.Length; i++)
            {
                for (var d = 0; d < Dimention; d++)
                {
                    expr[i].Parameters[this.args[d]] = args[d];
                }
            }

            // calculate expression with given values
            var res = new double[expr.Length];
            for (var i = 0; i < expr.Length; i++)
            {
                res[i] = (double)expr[i].Evaluate();
            }

            return res;
        }
    }
}
