﻿using System;

namespace IntegralEquations.Math.Functions
{
    public class SymbolicScalarFunction : SymbolicVectorFunction, IScalarFunction
    {
        /// <summary>
        /// Multivariable scalar symbolic function evaluator
        /// For dim = n will be x1, x2, ..., xn arguments 
        /// </summary>
        /// <param name="expr">Symbolic expressions to evaluate</param>
        /// <param name="dim">Number of variables</param>
        /// <param name="arg">Name of variable. By default 'x'</param>
        public SymbolicScalarFunction(string expr, int dim, string arg = "x") : base(new[] { expr }, dim, arg) { }

        public new double Val(double[] args)
        {
            return base.Val(args)[0];
        }
    }
}
