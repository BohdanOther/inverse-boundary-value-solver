﻿namespace IntegralEquations.Math
{
    public static class SpecialFunctions
    {
        /// <summary>
        /// Modified Bessel function of 0 order
        /// </summary>
        /// <returns>NaN if x = 0</returns>
        public static double BesselK0(double x)
        {
            return MathNet.Numerics.SpecialFunctions.BesselK0(x);
        }

        /// <summary>
        /// Modified Bessel function of 1 order
        /// </summary>
        public static double BesselK1(double x)
        {
            return MathNet.Numerics.SpecialFunctions.BesselK1(x);
        }

        /// <summary>
        /// Modified Bessel function of n-th order
        /// K[n+1](x) = (2n/x) * K[n](x) + K[n-1](x)
        /// source: http://www.aip.de/groups/soe/local/numres/bookcpdf/c6-6.pdf
        /// </summary>
        public static double BesselKn(double x, int n)
        {
            switch (n)
            {
                case 0:
                    return BesselK0(x);
                case 1:
                    return BesselK1(x);
                default:
                    if (n >= 2)
                    {
                        // recursion (2.0 * (n - 1)) / x * BessilKn(x, n - 1) + BessilKn(x, n - 2)
                        // is much slower and can couse overflow

                        var twoOverX = 2.0 / x;

                        var bkm = BesselK0(x);
                        var bk = BesselK1(x);

                        for (var j = 1; j < n; j++)
                        {
                            var bkp = j * twoOverX * bk + bkm;
                            bkm = bk;
                            bk = bkp;
                        }
                        return bk;
                    }
                    break;
            }

            return double.NaN; // n < 0
        }

        public static double BesselI0(double x)
        {
            return MathNet.Numerics.SpecialFunctions.BesselI0(x);
        }

        public static double BesselI1(double x)
        {
            return MathNet.Numerics.SpecialFunctions.BesselI1(x);
        }

        public static double Sigma0(double z)
        {
            var s = System.Math.Log(2) - Constants.Gamma;
            var d = 1.0;
            var d1 = s;
            var sigma = s;
            var k = 1;
            while (d1 >= Constants.Eps)
            {
                d = d * (z * z / 4) / (k * 2);
                s = s + 1.0 / k;
                d1 = s * d;
                sigma = sigma + d1;
                k = k + 1;
            }
            return sigma;
        }

        /// <summary>
        /// Discrete version of singular value of ln(sin(0))
        /// </summary>
        /// <param name="M">Size of mesh</param>
        public static double R(double t, double tj, int M)
        {
            var sum = 0.0;
            for (var m = 1; m < M; m++)
            {
                sum += (System.Math.Cos(m * (t - tj)) / m);
            }

            // divide by M?
            return -2.0d * sum - System.Math.Cos(M * (t - tj)) / M;
        }

        /// <summary>
        /// Fundamental solution of Klein-Gordon equation
        /// x != y
        /// </summary>
        public static double Phi(double[] x, double[] y, double k = 1.0)
        {
            var r = FunctionHelpers.EuclidianDistance(x, y);
            return BesselK0(k * r) / Constants.Pi2;
        }

        public static double LaplaceFundamentalSolution(double[] x, double[] y)
        {
            var r = FunctionHelpers.EuclidianDistance(x, y);
            return -System.Math.Log(r) / Constants.Pi2;
        }

        public static double LaplaceFundamentalSolutionNormalDerivative(double[] x, double[] y, double[] normal)
        {
            var r = FunctionHelpers.EuclidianDistance(x, y);
            var xMinusY = FunctionHelpers.VectorDifference(x, y);

            return -FunctionHelpers.ScalarProduct(xMinusY, normal) / (Constants.Pi2 * r * r);
        }
    }
}
